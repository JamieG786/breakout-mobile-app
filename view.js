"use strict";

function view() {
    var canvas = document.getElementById("canvas");
    var c = canvas.getContext("2d");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    var centerWidth = canvas.width / 3,
        centerHeight = canvas.height / 2,
        paddleWidth = (window.innerWidth / 4) - 3,
        paddleHeight = ((window.innerHeight / 2) / 4) / 5 - 3,
        paddleStart = (canvas.width - paddleWidth) / 2,
        x = Math.random() * window.innerWidth,
        y = centerHeight,
        radius = window.innerHeight / 80,
        dx = 3,
        dy = 3,
        rows = 5,
        columns = 5,
        brickPadding = 5,
        brickWidth = (window.innerWidth / 5) - brickPadding,
        brickHeight = ((window.innerHeight / 2) / 4)/2 - brickPadding,
        brickCount = rows * columns,
        score = 0;

    var bricks = [];
    for (var col = 0; col < columns; col++) {
        bricks[col] = [];
        for (var r = 0; r < rows; r++) {
            bricks[col][r] = {x: 0, y: 0, status: 1};
        }
    }


    this.clear = function () {
        c.clearRect(0, 0, canvas.width, canvas.height);
    };


    this.drawPaddle = function () {
        c.beginPath();
        c.rect(paddleStart, canvas.height - paddleHeight, paddleWidth, paddleHeight);
        c.fillStyle = "black";
        c.fill();
        c.closePath();
        c.beginPath();
        c.font = "30px Arial";
        c.fillText("Score: " + score, 0, centerHeight);
        c.closePath();

    };


    this.movePaddle = function () {
        window.addEventListener("deviceorientation", function (event) {
            var g = event.gamma;
            var dx = centerWidth + (g * 3);
            if (dx < window.innerWidth - paddleWidth && dx > 0) {
                paddleStart = dx;
            }
        });
    };

    this.drawCircle = function () {
        c.beginPath();
        c.arc(x, y, radius, 0, Math.PI * 2, false);
        c.fillStyle = "grey";
        c.fill();
        c.stroke();
    };

    this.moveCircle = function () {
        if (x + radius > window.innerWidth || x - radius < 0) {
            dx = -dx;
        }
        if (y + radius > window.innerHeight || y - radius < 0) {
            dy = -dy;
        }
        x += dx;
        y += dy;

    };

    this.drawBricks = function () {
        for (var col = 0; col < columns; col++) {
            for (var r = 0; r < rows; r++) {
                if (bricks[col][r].status === 1) {
                    var brickX = (col * (brickWidth + brickPadding));
                    var brickY = (r * (brickHeight + brickPadding));
                    bricks[col][r].x = brickX;
                    bricks[col][r].y = brickY;
                    c.beginPath();
                    c.rect(brickX, brickY, brickWidth, brickHeight);
                    c.fillStyle = "green";
                    c.fill();
                    c.closePath();
                }
            }
        }
    };

    this.collisionDetection = function () {
        for (var col = 0; col < columns; col++) {
            for (var r = 0; r < rows; r++) {
                var b = bricks[col][r];
                if (b.status === 1) {
                    if (y > b.y && y < b.y + brickHeight && x > b.x + brickWidth + radius && x + dx < b.x + brickWidth + radius) {
                        dx = -dx;
                        b.status = 0;
                        brickCount -= 1;
                        score += 1;
                    } else if (y > b.y && y < b.y + brickHeight && x < b.x - radius && x + dx > b.x - radius) {
                        dx = -dx;
                        b.status = 0;
                        brickCount -= 1;
                        score += 1;
                    } else if (x + dx > b.x + radius && x + dx < b.x + radius + brickWidth && y + dy > b.y - radius && y + dy < b.y + radius + brickHeight) {
                        dy = -dy;
                        b.status = 0;
                        brickCount -= 1;
                        score += 1;
                    }
                }
            }
        }
        if (x + radius > paddleStart && x - radius < (paddleStart + paddleWidth) && y + radius >= (canvas.height - paddleHeight)) {
            dy = -dy;
        }
    };

    this.checkGame = function () {
        if (brickCount === 0) {
            return 1;
        } else if (y + radius > window.innerHeight) {
            return 2;
        } else {
            return 3;
        }
    };

    this.resetValues = function () {
        brickCount = rows * columns;
        score = 0;
        for (var col = 0; col < columns; col++) {
            bricks[col] = [];
            for (var r = 0; r < rows; r++) {
                bricks[col][r] = {x: 0, y: 0, status: 1};
            }
        }
        x = Math.random() * window.innerWidth;
        y = centerHeight;
        this.xCheck();
    };

    this.xCheck = function () {
            if (x < 5 || x > window.innerWidth - 10) {
                x = Math.random() * window.innerWidth;
                if (x < 5 || x > window.innerWidth - 10) {
                    x = Math.random() * window.innerWidth;
                }
            }
    };

    this.drawHome = function() {
        c.beginPath();
        c.font = "25px Arial";
        c.fillText("Welcome to Breakout!", 0, 25);
        c.closePath();
        c.beginPath();
        c.font = "25px Arial";
        c.fillText("Click the buttons above to begin...", 0, 50);
        c.closePath();
    };

    this.setDifficulty = function(difficulty) {
        dx = difficulty;
        dy = -difficulty;
    };
}




