# Breakout Mobile App

A mobile app which was created for the CS312 class at the University of Strathclyde.
This is an app which uses the gamma sensors from a mobile phone to move the paddle across the screen.
If the ball reaches the bottom of the screen the user recieves an alert and the game is restarted.
If the user breaks all the blocks on the screen they recieve an alert that they have completed the game and the game is restarted.
The game has 4 different difficulties which vary the speed of which the ball moves at.

This app is hosted on the University of Strathclyde DevWeb server here: https://devweb2019.cis.strath.ac.uk/~rqb17144/Breakout/

![](Breakout.gif)