"use strict";
/*Globals view, model*/

var brickView = new view(),
    brickController = null,
    startGame = false,
    difficulty = 3;

function controller() {

    this.init = function() {
        brickView.xCheck();
        brickView.clear();
        brickView.drawHome();
        brickController.draw();

    };

    this.draw = function() {
        if (startGame === true) {

            brickView.clear();
            brickView.drawPaddle();
            if (window.DeviceOrientationEvent) {
                brickView.movePaddle();
            }
            brickView.drawCircle();
            brickView.moveCircle();
            brickView.collisionDetection();
            brickView.drawBricks();
        }
            if (brickView.checkGame() === 1) {
                alert("Well done you completed Breakout!");
                brickView.resetValues();
                brickController.restartGame();
            } else if (brickView.checkGame() === 2) {
                alert("Game over! Try again");
                brickView.resetValues();
                brickController.restartGame();
            } else if (brickView.checkGame() === 3) {
                requestAnimationFrame(brickController.draw);
            }

    };

    this.restartGame = function() {
        brickController.draw();
    };
}

brickController = new controller();
window.addEventListener("load", brickController.init());

function handleEnableClick() {
    if (window.DeviceOrientationEvent) {
        if (typeof DeviceOrientationEvent.requestPermission === 'function') {
            DeviceOrientationEvent.requestPermission();
        }

        if (window.DeviceMotionEvent) {
            if (typeof DeviceMotionEvent.requestPermission === 'function') {
                DeviceMotionEvent.requestPermission();
            }
        }
        document.getElementById("enable").style.display = "none";
    }
}

function easy() {
    startGame = true;
    document.getElementById("text").style.display = "none";
    document.getElementById("easy").style.display = "none";
    document.getElementById("medium").style.display = "none";
    document.getElementById("hard").style.display = "none";
    document.getElementById("impossible").style.display = "none";
    brickView.setDifficulty(difficulty);
}

function medium() {
    startGame = true;
    difficulty = 5;
    document.getElementById("text").style.display = "none";
    document.getElementById("easy").style.display = "none";
    document.getElementById("medium").style.display = "none";
    document.getElementById("hard").style.display = "none";
    document.getElementById("impossible").style.display = "none";
    brickView.setDifficulty(difficulty);
}

function hard() {
    startGame = true;
    difficulty = 8;
    document.getElementById("text").style.display = "none";
    document.getElementById("easy").style.display = "none";
    document.getElementById("medium").style.display = "none";
    document.getElementById("hard").style.display = "none";
    document.getElementById("impossible").style.display = "none";
    brickView.setDifficulty(difficulty);
}


function impossible() {
    startGame = true;
    difficulty = 10;
    document.getElementById("text").style.display = "none";
    document.getElementById("easy").style.display = "none";
    document.getElementById("medium").style.display = "none";
    document.getElementById("hard").style.display = "none";
    document.getElementById("impossible").style.display = "none";
    brickView.setDifficulty(difficulty);
}

